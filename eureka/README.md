# Eureka Registry Server

## Build and Run

### Build
```
mvnw clean install -U
```

### Run
Be sure u have set JAVA_HOME to right JDK path.

Execute command for local testing:
```
mvn spring-boot:run
```
or

```
java -Xmx256m -jar $PATH_TO_JAR
```

You can access running instance by URL:
[http://localhost:8761](http://localhost:8761)

You should be able to see Eureka service dashboard.