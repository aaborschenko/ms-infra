# Gateway Proxy/Balancer Server

## Build and Run

### Build
```
mvnw clean install -U

```

### Run
Be sure u have set JAVA_HOME to right JDK path.

Execute command for local testing:
```
mvn spring-boot:run
```
or

```
java -Xmx256m -jar $PATH_TO_JAR
```

You can access running instance by URL:
[http://localhost:8762](http://localhost:8762)

## Management URLs
Management endpoints enabled for this service.

To access list of available management endpoints use this URL:

[http://localhost:8762/actuator](http://localhost:8762/actuator)

You will get JSON data with all available management endpoints listed.

```json
    {
      "_links": {
        "self": {
          "href": "http://localhost:8762/actuator",
          "templated": false
        },
        "archaius": {
          "href": "http://localhost:8762/actuator/archaius",
          "templated": false
        },
        "auditevents": {
          "href": "http://localhost:8762/actuator/auditevents",
          "templated": false
        },
        "beans": {
          "href": "http://localhost:8762/actuator/beans",
          "templated": false
        },
        "health": {
          "href": "http://localhost:8762/actuator/health",
          "templated": false
        },
        "conditions": {
          "href": "http://localhost:8762/actuator/conditions",
          "templated": false
        },
        "configprops": {
          "href": "http://localhost:8762/actuator/configprops",
          "templated": false
        },
        "env": {
          "href": "http://localhost:8762/actuator/env",
          "templated": false
        },
        "env-toMatch": {
          "href": "http://localhost:8762/actuator/env/{toMatch}",
          "templated": true
        },
        "info": {
          "href": "http://localhost:8762/actuator/info",
          "templated": false
        },
        "logfile": {
          "href": "http://localhost:8762/actuator/logfile",
          "templated": false
        },
        "loggers": {
          "href": "http://localhost:8762/actuator/loggers",
          "templated": false
        },
        "loggers-name": {
          "href": "http://localhost:8762/actuator/loggers/{name}",
          "templated": true
        },
        "heapdump": {
          "href": "http://localhost:8762/actuator/heapdump",
          "templated": false
        },
        "threaddump": {
          "href": "http://localhost:8762/actuator/threaddump",
          "templated": false
        },
        "metrics-requiredMetricName": {
          "href": "http://localhost:8762/actuator/metrics/{requiredMetricName}",
          "templated": true
        },
        "metrics": {
          "href": "http://localhost:8762/actuator/metrics",
          "templated": false
        },
        "scheduledtasks": {
          "href": "http://localhost:8762/actuator/scheduledtasks",
          "templated": false
        },
        "httptrace": {
          "href": "http://localhost:8762/actuator/httptrace",
          "templated": false
        },
        "mappings": {
          "href": "http://localhost:8762/actuator/mappings",
          "templated": false
        },
        "refresh": {
          "href": "http://localhost:8762/actuator/refresh",
          "templated": false
        },
        "features": {
          "href": "http://localhost:8762/actuator/features",
          "templated": false
        },
        "service-registry": {
          "href": "http://localhost:8762/actuator/service-registry",
          "templated": false
        },
        "routes": {
          "href": "http://localhost:8762/actuator/routes",
          "templated": false
        },
        "routes-format": {
          "href": "http://localhost:8762/actuator/routes/{format}",
          "templated": true
        },
        "filters": {
          "href": "http://localhost:8762/actuator/filters",
          "templated": false
        },
        "hystrix.stream": {
          "href": "http://localhost:8762/actuator/hystrix.stream",
          "templated": false
        }
      }
    }
```

## Listing Registered Service Routes

Every service registered at **Eureka** server will be listed here as endpoints. We call them **routes**.

To get list of available **routes** got to URL:

[http://localhost:8762/actuator/routes](http://localhost:8762/actuator/routes)